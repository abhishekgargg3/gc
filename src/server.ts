import App from './app'
import 'dotenv/config';
import validateEnv from './utils/validateEnv';

import * as bodyParser from 'body-parser'
import * as cookieParser from 'cookie-parser';
import loggerMiddleware from './middleware/logger'
import authMiddleware from './middleware/auth.middleware'

import HomeController from './controllers/home/home.controller'
import UserController from './controllers/users/users.controller'
import AuthController from './controllers/authentication/authentication.controller'

validateEnv();

const { PORT } = process.env;

const app = new App({
    port: +PORT,
    controllers: [
        new HomeController(),
        new UserController(),
        new AuthController()
    ],
    middleWares: [
        bodyParser.json(),
        cookieParser(),
        bodyParser.urlencoded({ extended: true }),
        loggerMiddleware,
        authMiddleware
    ]
})

app.listen()