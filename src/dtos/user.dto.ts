import { IsString, IsBoolean, IsInt } from 'class-validator';
import userModel from 'models/user/user.model';
 
class CreateUserDto {
  @IsString()
  public username: string;
 
  @IsString()
  public password: string;
 
  @IsString()
  public firstName: string;

  @IsString()
  public lastName: string;

  @IsInt()
  public mobile: number;

  @IsBoolean()
  public isActive: boolean;

  constructor(username: string, password: string, firstName: string, lastName: string, mobile: number, isActive: boolean){
    this.username = username;
    this.firstName = firstName;
    this.lastName = lastName;
    this.mobile = mobile;
    this.isActive = isActive;
    this.password = password;
  }
}
 
export default CreateUserDto;