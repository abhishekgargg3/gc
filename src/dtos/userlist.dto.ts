import { IsArray, ValidateNested } from 'class-validator';
import CreateUserDto from './user.dto';
 
class CreateUserListDto {
    @IsArray()
    @ValidateNested({ each: true })
    public users: CreateUserDto[];

    constructor(users: CreateUserDto[]) {
        this.users = users;
      }
}
 
export default CreateUserListDto;