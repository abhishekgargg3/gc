import * as express from 'express'
import * as jwt from 'jsonwebtoken';
import { Request, Response, NextFunction } from 'express'
import LogInDto from '../../dtos/userlogin.dto'
import DataStoredInToken from '../../interfaces/dataStoredInToken'
import TokenData from '../../interfaces/tokenData.interface'
import IControllerBase from '../../interfaces/IControllerBase.interface'
import { validationMiddleware } from '../../middleware/validation.middleware'
import ListUploadService from '../../service/listupload.service'
import AuthenticationService from '../../service/authentication.service'

class AuthController implements IControllerBase {
    public path = '/login'
    public router = express.Router()

    constructor() {
        this.initRoutes()
    }

    public initRoutes() {
        this.router.post(this.path, validationMiddleware(LogInDto), this.loggingIn);
    }

    private loggingIn = async (request: Request, response: Response, next: NextFunction) => {
        const logInData: LogInDto = request.body
        const latestListUpload = await ListUploadService.findLatestListUpload()
        const res = await AuthenticationService.authenticateUser(logInData, latestListUpload)
        if(res.error) {
            next(res.error)
        } else {
            const tokenData = this.createToken(res.user);
            response.setHeader('Set-Cookie', [this.createCookie(tokenData)]);
            response.send(res.user)
        }
    }

    private createToken(user: any): TokenData {
        const expiresIn = 60 * 60;
        const secret = process.env.JWT_SECRET;
        const dataStoredInToken: DataStoredInToken = {
            _id: user._id,
        };
        return {
            expiresIn,
            token: jwt.sign(dataStoredInToken, secret, { expiresIn }),
        };
    }

    private createCookie(tokenData: TokenData) {
        return `Authorization=${tokenData.token}; HttpOnly; Max-Age=${tokenData.expiresIn}`;
    }
}

export default AuthController