import * as express from 'express'
import { Request, Response, NextFunction } from 'express'
import User from '../../models/user/user.interface'
import userModel from '../../models/user/user.model'
import IControllerBase from '../../interfaces/IControllerBase.interface'
import HttpException from './../../exceptions/HttpException'
import CreateUserDto from '../../dtos/user.dto'
import { validationMiddleware, validateObject } from '../../middleware/validation.middleware'

import UserList from '../../models/user/userlist.interface'
import CreateUserListDto from '../../dtos/userlist.dto'
import ListUploadService from '../../service/listupload.service'
import UserService from '../../service/user.service'
import S3Service from '../../service/aws/S3Service'

import authMiddleware from '../../middleware/auth.middleware'

class UserController implements IControllerBase {
    public path = '/users'
    public router = express.Router()
    public s3Service: S3Service

    constructor() {
        this.initRoutes()
        this.s3Service = new S3Service()
    }

    public initRoutes() {
        this.router.get(this.path, authMiddleware, this.getAllUsers)
        this.router.post(this.path, validationMiddleware(CreateUserDto), this.createUser)
        this.router.post(this.path + '/upload', validationMiddleware(CreateUserListDto), this.uploadUsersList)
    }

    getAllUsers = (req: Request, res: Response, next: NextFunction) => {
        userModel.find()
        .then(users => {
            res.send(users);
        })
        .catch(ex => {
            console.error(ex)
            next(new HttpException(502, 'Could not fetch users.'))
        })
    }

    createUser = async (req: Request, res: Response, next: NextFunction) => {
        const userData: User = req.body;
        const user = await UserService.createUser(userData)
        res.send(user);
    }

    uploadUsersList = async (req: Request, res: Response, next: NextFunction) => {
        const userList: UserList = req.body;
        const promises = userList.users.map(async (user) => {
            return await validateObject(CreateUserDto, user)
        })
        try {
            await Promise.all(promises)
        } catch (ex) {
            return res.status(ex.status).send({ error: ex.message })
        }
        const listUploadItem = await ListUploadService.createListUpload(userList.users.length)
        
        UserService.createUsers(userList.users, listUploadItem._id) // this is async creation of users
        
        const dataForS3 = userList.users.map(user => { return {isActive: user.isActive, userName: user.username} })
        this.s3Service.upload(null, dataForS3); // this is async uploading on s3
        
        res.send(200)
    }
}

export default UserController