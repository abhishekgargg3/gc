import * as express from 'express'
import { Application } from 'express'
import * as mongoose from 'mongoose';
import errorMiddleware from './middleware/error.middleware';

class App {
    public app: Application
    public port: number

    constructor(appInit: { port: number; middleWares: any; controllers: any; }) {
        this.app = express()
        this.port = appInit.port

        this.connectToTheDatabase()
        this.middlewares(appInit.middleWares)
        this.routes(appInit.controllers)
        this.initializeErrorHandling()
    }

    private initializeErrorHandling() {
        this.app.use(errorMiddleware)
    }

    private middlewares(middleWares) {
        middleWares.forEach(middleWare => {
            this.app.use(middleWare)
        })
    }

    private routes(controllers) {
        controllers.forEach(controller => {
            this.app.use('/', controller.router)
        })
    }

    public listen() {
        this.app.listen(this.port, () => {
            console.log(`App listening on the http://localhost:${this.port}`)
        })
    }

    public connectToTheDatabase() {
        const { MONGO_AUTH_REQUIRED, MONGO_USER, MONGO_PASSWORD, MONGO_PATH } = process.env;
        let mongoConnectUrl: string;

        if(MONGO_AUTH_REQUIRED) {
            mongoConnectUrl = `mongodb://${MONGO_USER}:${MONGO_PASSWORD}@${MONGO_PATH}`;
        } else {
            mongoConnectUrl = `mongodb://${MONGO_PATH}`;
        }
        
        mongoose.connect(mongoConnectUrl, { 
            useCreateIndex: true,
            useNewUrlParser: true,
            useFindAndModify: false, 
            useUnifiedTopology: true
        })
      }
}

export default App