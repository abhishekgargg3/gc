interface ListUpload {
    created: Date
    totalRecords: number
}

export default ListUpload