import * as mongoose from 'mongoose'
import ListUpload from './listupload.interface'

const listUploadSchema = new mongoose.Schema({
    created: Date,
    totalRecords: Number
});
 
const listUploadModel = mongoose.model<ListUpload & mongoose.Document>('ListUpload', listUploadSchema);
 
export default listUploadModel;