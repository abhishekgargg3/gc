interface User {
    username: string
    password: string
    firstname: string
    lastname: string
    mobile: number
    isActive: boolean
}

export default User