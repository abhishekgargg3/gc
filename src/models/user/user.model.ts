import * as mongoose from 'mongoose'
import User from './user.interface'

const userSchema = new mongoose.Schema({
    username: String,
    password: String,
    firstName: String,
    lastName: String,
    mobile: Number,
    isActive: Boolean,
    listUploadId: String
});
 
const userModel = mongoose.model<User & mongoose.Document>('User', userSchema);
 
export default userModel;