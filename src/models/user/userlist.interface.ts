import User from './user.interface'

interface UserList {
    users: User[]
}

export default UserList