import listUploadModel from '../models/listupload/listupload.model'

class ListUploadService {

    public static createListUpload = async (userListLength) => {
        const saveListUpload = new listUploadModel({
            created: new Date(),
            totalRecords: userListLength
        })
        const savedListUpload = await saveListUpload.save()
        return savedListUpload
    }

    public static findLatestListUpload = async () => {
        const listUpload = await listUploadModel.findOne().sort({created: -1})
        return listUpload._id
    }

}

export default ListUploadService