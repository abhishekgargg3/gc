
import * as bcrypt from 'bcrypt';
import userModel from './../models/user/user.model'

class UserService {

    public static createUser = async (userData) => {
        const hashedPassword = await bcrypt.hash(userData.password, 10);
        const saveUser = new userModel({
            ...userData,
            password: hashedPassword,
        })
        const savedUser = await saveUser.save()
        savedUser.password = undefined
        return savedUser
    }

    public static createUsers = (usersList, listUploadId) => {
        usersList.forEach(user => {
            UserService.createUser({
                ...user,
                listUploadId
            })
        });
    }

}

export default UserService