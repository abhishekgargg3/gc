import * as bcrypt from 'bcrypt';
import listUploadModel from '../models/listupload/listupload.model'
import userModel from '../models/user/user.model'
import HttpException from './../exceptions/HttpException';

class AuthenticationService {

    public static createListUpload = async (userListLength) => {
        const saveListUpload = new listUploadModel({
            created: new Date(),
            totalRecords: userListLength
        })
        const savedListUpload = await saveListUpload.save()
        return savedListUpload
    }

    public static authenticateUser = async (logInData, latestListUpload) => {
        const user = await userModel.findOne({ username: logInData.username, listUploadId: latestListUpload, isActive: true })
        if (user) {
            const isPasswordMatching = await bcrypt.compare(logInData.password, user.password);
            if (isPasswordMatching) {
                user.password = undefined;
                return { user, error: null };
            } else {
                return { user: null, error: new HttpException(502, 'Incorrect password entered') };
            }
        } else {
            return { user: null, error: new HttpException(502, 'User not found with username in the latest list upload.') };
        }
    }

}

export default AuthenticationService