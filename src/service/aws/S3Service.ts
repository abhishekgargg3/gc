import * as AWS from 'aws-sdk'
import * as fs from 'fs'
import * as path from 'path'
import HttpException from '../../exceptions/HttpException'

class S3Service {
    public s3: AWS.S3;

    constructor() {
        this.s3 = new AWS.S3({
            accessKeyId: process.env.S3_ACCESS_KEY,
            secretAccessKey: process.env.S3_SECRET_ACCESS_KEY
        });
    }

    public upload(bucketName: string, data: any) {

        const filePath = './userListFileUpload.txt'
        fs.writeFileSync(filePath, JSON.stringify(data), 'utf8');

        const fileContent = fs.readFileSync(filePath);
        
        const params = {
            Bucket: bucketName || 'abhi-first-bucket-030690',
            Body : fileContent,
            Key : "userList/"+Date.now()+"_"+path.basename(filePath)
        };
        
        this.s3.upload(params, function (err, data) {
            if (err) {
                console.log("Error", err);
                return new HttpException(502, "Error occured while uploading on s3")
            }
        
            if (data) {
                console.log("Uploaded in:", data.Location);
                return data
            }
        });
        
    }
}

export default S3Service