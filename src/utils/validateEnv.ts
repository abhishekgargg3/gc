import { cleanEnv, str, port, bool } from 'envalid';
   
function validateEnv() {
    cleanEnv(process.env, {
        MONGO_AUTH_REQUIRED: bool(),
        MONGO_PASSWORD: str(),
        MONGO_PATH: str(),
        MONGO_USER: str(),
        PORT: port(),
    });
}

export default validateEnv;