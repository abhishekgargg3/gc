## Requirements

node version 12.14.0

running mongodb on local without authentication. If authentication required, then change params in environment vars.

## Installation

```bash
npm i
```
```bash
npm run-script build
```
```bash
npm run-script start
```

## Postman APIs

**API to update list of users**

curl --location --request POST 'http://localhost:5000/users/upload' \
--header 'Content-Type: application/json' \
--data-raw '{
	"users": [
		{
			"username": "abhishekgargg",
			"firstName": "Abhishek",
			"lastName": "Garg",
			"password": "1234asdf",
			"isActive": true,
			"mobile": 9875873674
		},
		{
			"username": "abhishekgargg3",
			"firstName": "Abhi",
			"lastName": "Garg",
			"password": "1234asdf",
			"isActive": false,
			"mobile": 9875873674
		}
	]
}'


**API to login**

curl --location --request POST 'http://localhost:5000/login' \
--header 'Content-Type: application/json' \
--data-raw '{
	"username": "abhishekgargg",
	"password": "1234asdf"
}'


**API to get list of all users**

curl -XGET 'http://localhost:5000/users'
