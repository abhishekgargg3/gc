"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const class_transformer_1 = require("class-transformer");
const class_validator_1 = require("class-validator");
const HttpException_1 = require("../exceptions/HttpException");
function validationMiddleware(type) {
    return (req, res, next) => {
        class_validator_1.validate(class_transformer_1.plainToClass(type, req.body))
            .then((errors) => {
            if (errors.length > 0) {
                const message = errors.map((error) => Object.values(error.constraints)).join(', ');
                next(new HttpException_1.default(400, message));
            }
            else {
                next();
            }
        });
    };
}
exports.validationMiddleware = validationMiddleware;
function validateObject(type, obj) {
    return __awaiter(this, void 0, void 0, function* () {
        var errors = yield class_validator_1.validate(class_transformer_1.plainToClass(type, obj));
        if (errors.length > 0) {
            const message = errors.map((error) => Object.values(error.constraints)).join(', ');
            throw new HttpException_1.default(400, message);
        }
        else {
            return { response: true, error: null };
        }
    });
}
exports.validateObject = validateObject;
//# sourceMappingURL=validation.middleware.js.map