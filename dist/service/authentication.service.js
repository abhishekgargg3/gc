"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const bcrypt = require("bcrypt");
const listupload_model_1 = require("../models/listupload/listupload.model");
const user_model_1 = require("../models/user/user.model");
const HttpException_1 = require("./../exceptions/HttpException");
class AuthenticationService {
}
AuthenticationService.createListUpload = (userListLength) => __awaiter(void 0, void 0, void 0, function* () {
    const saveListUpload = new listupload_model_1.default({
        created: new Date(),
        totalRecords: userListLength
    });
    const savedListUpload = yield saveListUpload.save();
    return savedListUpload;
});
AuthenticationService.authenticateUser = (logInData, latestListUpload) => __awaiter(void 0, void 0, void 0, function* () {
    const user = yield user_model_1.default.findOne({ username: logInData.username, listUploadId: latestListUpload, isActive: true });
    if (user) {
        const isPasswordMatching = yield bcrypt.compare(logInData.password, user.password);
        if (isPasswordMatching) {
            user.password = undefined;
            return { user, error: null };
        }
        else {
            return { user: null, error: new HttpException_1.default(502, 'Incorrect password entered') };
        }
    }
    else {
        return { user: null, error: new HttpException_1.default(502, 'User not found with username in the latest list upload.') };
    }
});
exports.default = AuthenticationService;
//# sourceMappingURL=authentication.service.js.map