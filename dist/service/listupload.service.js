"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const listupload_model_1 = require("../models/listupload/listupload.model");
class ListUploadService {
}
ListUploadService.createListUpload = (userListLength) => __awaiter(void 0, void 0, void 0, function* () {
    const saveListUpload = new listupload_model_1.default({
        created: new Date(),
        totalRecords: userListLength
    });
    const savedListUpload = yield saveListUpload.save();
    return savedListUpload;
});
ListUploadService.findLatestListUpload = () => __awaiter(void 0, void 0, void 0, function* () {
    const listUpload = yield listupload_model_1.default.findOne().sort({ created: -1 });
    return listUpload._id;
});
exports.default = ListUploadService;
//# sourceMappingURL=listupload.service.js.map