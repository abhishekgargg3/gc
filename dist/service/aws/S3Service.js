"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const AWS = require("aws-sdk");
const fs = require("fs");
const path = require("path");
const HttpException_1 = require("../../exceptions/HttpException");
class S3Service {
    constructor() {
        this.s3 = new AWS.S3({
            accessKeyId: process.env.S3_ACCESS_KEY,
            secretAccessKey: process.env.S3_SECRET_ACCESS_KEY
        });
    }
    upload(bucketName, data) {
        const filePath = './userListFileUpload.txt';
        fs.writeFileSync(filePath, JSON.stringify(data), 'utf8');
        const fileContent = fs.readFileSync(filePath);
        const params = {
            Bucket: bucketName || 'abhi-first-bucket-030690',
            Body: fileContent,
            Key: "userList/" + Date.now() + "_" + path.basename(filePath)
        };
        this.s3.upload(params, function (err, data) {
            if (err) {
                console.log("Error", err);
                return new HttpException_1.default(502, "Error occured while uploading on s3");
            }
            if (data) {
                console.log("Uploaded in:", data.Location);
                return data;
            }
        });
    }
}
exports.default = S3Service;
//# sourceMappingURL=S3Service.js.map