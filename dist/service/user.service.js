"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const bcrypt = require("bcrypt");
const user_model_1 = require("./../models/user/user.model");
class UserService {
}
UserService.createUser = (userData) => __awaiter(void 0, void 0, void 0, function* () {
    const hashedPassword = yield bcrypt.hash(userData.password, 10);
    const saveUser = new user_model_1.default(Object.assign(Object.assign({}, userData), { password: hashedPassword }));
    const savedUser = yield saveUser.save();
    savedUser.password = undefined;
    return savedUser;
});
UserService.createUsers = (usersList, listUploadId) => {
    usersList.forEach(user => {
        UserService.createUser(Object.assign(Object.assign({}, user), { listUploadId }));
    });
};
exports.default = UserService;
//# sourceMappingURL=user.service.js.map