"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const userSchema = new mongoose.Schema({
    username: String,
    password: String,
    firstName: String,
    lastName: String,
    mobile: Number,
    isActive: Boolean,
    listUploadId: String
});
const userModel = mongoose.model('User', userSchema);
exports.default = userModel;
//# sourceMappingURL=user.model.js.map