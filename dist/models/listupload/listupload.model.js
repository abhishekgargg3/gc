"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const listUploadSchema = new mongoose.Schema({
    created: Date,
    totalRecords: Number
});
const listUploadModel = mongoose.model('ListUpload', listUploadSchema);
exports.default = listUploadModel;
//# sourceMappingURL=listupload.model.js.map