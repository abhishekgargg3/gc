"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const app_1 = require("./app");
require("dotenv/config");
const validateEnv_1 = require("./utils/validateEnv");
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const logger_1 = require("./middleware/logger");
const auth_middleware_1 = require("./middleware/auth.middleware");
const home_controller_1 = require("./controllers/home/home.controller");
const users_controller_1 = require("./controllers/users/users.controller");
const authentication_controller_1 = require("./controllers/authentication/authentication.controller");
validateEnv_1.default();
const { PORT } = process.env;
const app = new app_1.default({
    port: +PORT,
    controllers: [
        new home_controller_1.default(),
        new users_controller_1.default(),
        new authentication_controller_1.default()
    ],
    middleWares: [
        bodyParser.json(),
        cookieParser(),
        bodyParser.urlencoded({ extended: true }),
        logger_1.default,
        auth_middleware_1.default
    ]
});
app.listen();
//# sourceMappingURL=server.js.map