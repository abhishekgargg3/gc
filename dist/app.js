"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const mongoose = require("mongoose");
const error_middleware_1 = require("./middleware/error.middleware");
class App {
    constructor(appInit) {
        this.app = express();
        this.port = appInit.port;
        this.connectToTheDatabase();
        this.middlewares(appInit.middleWares);
        this.routes(appInit.controllers);
        this.initializeErrorHandling();
    }
    initializeErrorHandling() {
        this.app.use(error_middleware_1.default);
    }
    middlewares(middleWares) {
        middleWares.forEach(middleWare => {
            this.app.use(middleWare);
        });
    }
    routes(controllers) {
        controllers.forEach(controller => {
            this.app.use('/', controller.router);
        });
    }
    listen() {
        this.app.listen(this.port, () => {
            console.log(`App listening on the http://localhost:${this.port}`);
        });
    }
    connectToTheDatabase() {
        const { MONGO_AUTH_REQUIRED, MONGO_USER, MONGO_PASSWORD, MONGO_PATH } = process.env;
        let mongoConnectUrl;
        if (MONGO_AUTH_REQUIRED) {
            mongoConnectUrl = `mongodb://${MONGO_USER}:${MONGO_PASSWORD}@${MONGO_PATH}`;
        }
        else {
            mongoConnectUrl = `mongodb://${MONGO_PATH}`;
        }
        mongoose.connect(mongoConnectUrl, {
            useCreateIndex: true,
            useNewUrlParser: true,
            useFindAndModify: false,
            useUnifiedTopology: true
        });
    }
}
exports.default = App;
//# sourceMappingURL=app.js.map