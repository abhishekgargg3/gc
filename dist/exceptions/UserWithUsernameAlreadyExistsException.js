"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const HttpException_1 = require("./HttpException");
class UserWithUsernameAlreadyExistsException extends HttpException_1.default {
    constructor(username) {
        super(400, `User with username ${username} already exists`);
    }
}
exports.default = UserWithUsernameAlreadyExistsException;
//# sourceMappingURL=UserWithUsernameAlreadyExistsException.js.map