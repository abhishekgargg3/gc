"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const userlist_dto_1 = require("../../dtos/userlist.dto");
const validation_middleware_1 = require("../../middleware/validation.middleware");
const user_service_1 = require("../../service/user.service");
const listupload_service_1 = require("../../service/listupload.service");
class ListUploadController {
    constructor() {
        this.path = '/upload';
        this.router = express.Router();
        this.uploadUsersList = (req, res, next) => {
            const userList = req.body;
            //TODO: sanity check of the entire list
            //TODO: create entry in listupload
            const listUploadItem = listupload_service_1.default.createListUpload(userList);
            console.log('listUploadItem id :', JSON.stringify(listUploadItem));
            userList.users.forEach((userData) => __awaiter(this, void 0, void 0, function* () {
                user_service_1.default.createUser(userData);
            }));
            res.send(200);
        };
        this.initRoutes();
    }
    initRoutes() {
        this.router.post(this.path, validation_middleware_1.default(userlist_dto_1.default), this.uploadUsersList);
    }
}
exports.default = ListUploadController;
//# sourceMappingURL=listupload.controller.js.map