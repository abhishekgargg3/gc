"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const jwt = require("jsonwebtoken");
const userlogin_dto_1 = require("../../dtos/userlogin.dto");
const validation_middleware_1 = require("../../middleware/validation.middleware");
const listupload_service_1 = require("../../service/listupload.service");
const authentication_service_1 = require("../../service/authentication.service");
class AuthController {
    constructor() {
        this.path = '/login';
        this.router = express.Router();
        this.loggingIn = (request, response, next) => __awaiter(this, void 0, void 0, function* () {
            const logInData = request.body;
            const latestListUpload = yield listupload_service_1.default.findLatestListUpload();
            const res = yield authentication_service_1.default.authenticateUser(logInData, latestListUpload);
            if (res.error) {
                next(res.error);
            }
            else {
                const tokenData = this.createToken(res.user);
                response.setHeader('Set-Cookie', [this.createCookie(tokenData)]);
                response.send(res.user);
            }
        });
        this.initRoutes();
    }
    initRoutes() {
        this.router.post(this.path, validation_middleware_1.validationMiddleware(userlogin_dto_1.default), this.loggingIn);
    }
    createToken(user) {
        const expiresIn = 60 * 60;
        const secret = process.env.JWT_SECRET;
        const dataStoredInToken = {
            _id: user._id,
        };
        return {
            expiresIn,
            token: jwt.sign(dataStoredInToken, secret, { expiresIn }),
        };
    }
    createCookie(tokenData) {
        return `Authorization=${tokenData.token}; HttpOnly; Max-Age=${tokenData.expiresIn}`;
    }
}
exports.default = AuthController;
//# sourceMappingURL=authentication.controller.js.map