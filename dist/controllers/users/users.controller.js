"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const user_model_1 = require("../../models/user/user.model");
const HttpException_1 = require("./../../exceptions/HttpException");
const user_dto_1 = require("../../dtos/user.dto");
const validation_middleware_1 = require("../../middleware/validation.middleware");
const userlist_dto_1 = require("../../dtos/userlist.dto");
const listupload_service_1 = require("../../service/listupload.service");
const user_service_1 = require("../../service/user.service");
const S3Service_1 = require("../../service/aws/S3Service");
const auth_middleware_1 = require("../../middleware/auth.middleware");
class UserController {
    constructor() {
        this.path = '/users';
        this.router = express.Router();
        this.getAllUsers = (req, res, next) => {
            user_model_1.default.find()
                .then(users => {
                res.send(users);
            })
                .catch(ex => {
                console.error(ex);
                next(new HttpException_1.default(502, 'Could not fetch users.'));
            });
        };
        this.createUser = (req, res, next) => __awaiter(this, void 0, void 0, function* () {
            const userData = req.body;
            const user = yield user_service_1.default.createUser(userData);
            res.send(user);
        });
        this.uploadUsersList = (req, res, next) => __awaiter(this, void 0, void 0, function* () {
            const userList = req.body;
            const promises = userList.users.map((user) => __awaiter(this, void 0, void 0, function* () {
                return yield validation_middleware_1.validateObject(user_dto_1.default, user);
            }));
            try {
                yield Promise.all(promises);
            }
            catch (ex) {
                return res.status(ex.status).send({ error: ex.message });
            }
            const listUploadItem = yield listupload_service_1.default.createListUpload(userList.users.length);
            user_service_1.default.createUsers(userList.users, listUploadItem._id); // this is async creation of users
            const dataForS3 = userList.users.map(user => { return { isActive: user.isActive, userName: user.username }; });
            this.s3Service.upload(null, dataForS3); // this is async uploading on s3
            res.send(200);
        });
        this.initRoutes();
        this.s3Service = new S3Service_1.default();
    }
    initRoutes() {
        this.router.get(this.path, auth_middleware_1.default, this.getAllUsers);
        this.router.post(this.path, validation_middleware_1.validationMiddleware(user_dto_1.default), this.createUser);
        this.router.post(this.path + '/upload', validation_middleware_1.validationMiddleware(userlist_dto_1.default), this.uploadUsersList);
    }
}
exports.default = UserController;
//# sourceMappingURL=users.controller.js.map