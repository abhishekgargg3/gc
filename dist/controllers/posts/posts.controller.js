"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const post_model_1 = require("../../posts/post.model");
class PostsController {
    constructor() {
        this.path = '/posts';
        this.router = express.Router();
        this.getAllPosts = (req, res) => {
            post_model_1.default.find()
                .then(posts => {
                res.send(posts);
            })
                .catch(ex => {
                console.error(ex);
                res.status(502);
            });
        };
        this.createPost = (req, res) => {
            const postData = req.body;
            const createdPost = new post_model_1.default(postData);
            createdPost.save()
                .then(savedPost => {
                res.send(savedPost);
            });
        };
        this.initRoutes();
    }
    initRoutes() {
        this.router.get(this.path, this.getAllPosts);
        this.router.post(this.path, this.createPost);
    }
}
exports.default = PostsController;
//# sourceMappingURL=posts.controller.js.map